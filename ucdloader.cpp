#include "ucdloader.h"
#include "ucd_zcomp.h"
#include <QBEInteger>

void UCDLoader::loadDB()
{
    QByteArray rawComp;
//    uint32_t beSize = qToBigEndian(ucd_txt_orig_size);
//    rawUncomp.append((ucd_txt_orig_size << 24) & 0xff);
//    rawUncomp.append((ucd_txt_orig_size << 16) & 0xff);
//    rawUncomp.append((ucd_txt_orig_size << 8) & 0xFF);
//    rawUncomp.append(ucd_txt_orig_size & 0xFF);
//    rawUncomp.append((beSize >> 24) & 0x000000FF);
//    rawUncomp.append((beSize >> 16) & 0x0000FF);
//    rawUncomp.append((beSize >> 8) & 0x00FF);
//    rawUncomp.append(beSize & 0xFF000000);
    rawComp.append((char*)ucd_txt_gz, ucd_txt_gz_len);
    QByteArray rawUncomp = qUncompress(rawComp);
    m_codePointNames = rawUncomp.split('\n').toVector();
    m_isLoaded = true;
}

const QString &UCDLoader::codePointName(uint16_t cp)
{
    static QString str;
    if (cp <= 0xFFFD) {
        str = m_codePointNames.at(cp);
        str = str.mid(1, str.size() - 2);
    }
    return str;
}

UCDLoader *ucdLoaderInstance()
{
    static UCDLoader instance;
    return &instance;
}
