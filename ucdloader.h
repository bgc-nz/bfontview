#ifndef UCDLOADER_H
#define UCDLOADER_H

#include <QObject>

class UCDLoader : public QObject
{
    Q_OBJECT
public:
    void loadDB();
    bool hasLoaded() { return m_isLoaded; }
    const QString& codePointName(uint16_t);
private:
    bool m_isLoaded=false;
    QVector<QByteArray> m_codePointNames;
};

UCDLoader* ucdLoaderInstance();

#endif // UCDLOADER_H
