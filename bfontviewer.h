#ifndef BFONTVIEWER_H
#define BFONTVIEWER_H

#include <QMainWindow>
#include <QRawFont>
#include <QAbstractItemModel>
#include <QListWidgetItem>
#include <QSharedPointer>
#include <QSortFilterProxyModel>
#include "rawfontrenderer.h"
#include "ucdloader.h"

QT_BEGIN_NAMESPACE
namespace Ui { class BFontViewer; }
QT_END_NAMESPACE

class BFontViewer : public QMainWindow
{
    Q_OBJECT

public:
    BFontViewer(QWidget *parent = nullptr);
    ~BFontViewer();
    enum UTFCategory
    {
        Mark_NonSpacing,          //   Mn
        Mark_SpacingCombining,    //   Mc
        Mark_Enclosing,           //   Me

        Number_DecimalDigit,      //   Nd
        Number_Letter,            //   Nl
        Number_Other,             //   No

        Separator_Space,          //   Zs
        Separator_Line,           //   Zl
        Separator_Paragraph,      //   Zp

        Other_Control,            //   Cc
        Other_Format,             //   Cf
        Other_Surrogate,          //   Cs
        Other_PrivateUse,         //   Co
        Other_NotAssigned,        //   Cn

        Letter_Uppercase,         //   Lu
        Letter_Lowercase,         //   Ll
        Letter_Titlecase,         //   Lt
        Letter_Modifier,          //   Lm
        Letter_Other,             //   Lo

        Punctuation_Connector,    //   Pc
        Punctuation_Dash,         //   Pd
        Punctuation_Open,         //   Ps
        Punctuation_Close,        //   Pe
        Punctuation_InitialQuote, //   Pi
        Punctuation_FinalQuote,   //   Pf
        Punctuation_Other,        //   Po

        Symbol_Math,              //   Sm
        Symbol_Currency,          //   Sc
        Symbol_Modifier,          //   Sk
        Symbol_Other              //   So
    };
    Q_ENUM(UTFCategory)
    const double FONT_SIZE = 56;
    void updateUTFCategoryComboBoxSelectedItem(int);
public slots:
    void findGlyphFromInputTerms();
private slots:
    void fontLoaded(RawFontGlyphModel* glyphsModel);
private:
    Ui::BFontViewer *ui;
    RawFontRenderer m_rawFontRenderer;
    QScopedPointer<QThread> m_rawFontRendererThread;
    QScopedPointer<QThread> m_ucdLoaderThread;
    QSortFilterProxyModel m_glyphModelFilter;
    // QWidget interface
protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
};
#endif // BFONTVIEWER_H
