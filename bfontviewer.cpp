#include "bfontviewer.h"
#include <QDebug>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QFileDialog>
#include <QMetaEnum>
#include <QMimeData>
#include <QPainter>
#include <QPainterPath>
#include <QStringListModel>
#include "ui_bfontviewer.h"
#include "unicoderanges.h"

BFontViewer::BFontViewer(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::BFontViewer)
{
    ui->setupUi(this);
    connect(ui->fontComboBox, &QFontComboBox::currentFontChanged, [&](const QFont& font){
        m_rawFontRenderer.loadFontFromSystem(font.family());
    });
    ui->listViewGlyphs->setModel(m_rawFontRenderer.model());
    ui->listViewGlyphs->setSelectionModel(
                new QItemSelectionModel(ui->listViewGlyphs->model())
                );
    connect(ui->listViewGlyphs->selectionModel(),
            &QItemSelectionModel::currentChanged,
            [&](const QModelIndex& selected, const QModelIndex& deselected) {
        Q_UNUSED(deselected);
        auto id = selected.row();
        RawFontGlyphModel* glyphModel = (RawFontGlyphModel*)ui->listViewGlyphs->model();
        RawFontGlyphModel::GlyphInfo gi = glyphModel->glyphInfoAt(id);
        updateUTFCategoryComboBoxSelectedItem(gi.codepoint);
        QPixmap glyphImage(256,256);
        QPainter p;
        QPainterPath glyphPath = glyphModel->glyphInfoAt(id).path;
        glyphImage.fill();
        p.begin(&glyphImage);
        p.setRenderHint(QPainter::RenderHint::Antialiasing);
        const double scale = 4;
        p.scale(scale,scale);
        p.translate(glyphImage.width()/**scale*//(2*scale)-glyphPath.boundingRect().center().x(),
                    glyphImage.height()/**scale*//(2*scale)-glyphPath.boundingRect().center().y());
        p.fillPath(glyphPath, QBrush(QColor(Qt::black)));
        p.end();
        ui->labelLiteral->setText(QString(gi.codepoint));
        ui->labelGlyph->setPixmap(std::move(glyphImage));
        QChar ch(gi.codepoint);
        if (ucdLoaderInstance()->hasLoaded()) {
            ui->labelDescription->setText(ucdLoaderInstance()->codePointName(gi.codepoint));
        }
        QMetaEnum catEnum = QMetaEnum::fromType<UTFCategory>();
        ui->labelCategory->setText(
                    QString(catEnum.valueToKey(ch.category())).replace('_',' ')
                    );
        ui->labelCodepoint->setText(QString::asprintf("0x%x", gi.codepoint));
    });
    connect(ui->treeViewResults,
            &QAbstractItemView::clicked,
            [this](const QModelIndex& selected) {
        ui->listViewGlyphs->selectionModel()->setCurrentIndex(m_glyphModelFilter.mapToSource(selected),
                                                              QItemSelectionModel::SelectionFlag::SelectCurrent);
        auto selection = m_glyphModelFilter.mapSelectionToSource(ui->treeViewResults->selectionModel()->selection());
        ui->listViewGlyphs->scrollTo(selection.indexes()[0]);
        ui->listViewGlyphs->selectionModel()->select(selection.indexes()[0],
                                                     QItemSelectionModel::SelectionFlag::Select);
        updateUTFCategoryComboBoxSelectedItem(selection.indexes()[0].row());
    });
    connect(ui->buttonFontPathBrowse, &QPushButton::pressed, [&]() {
        QString filePath
            = QFileDialog::getOpenFileName(this,
                                           "Select a font",
                                           QString(),
                                           "TrueType Fonts (*.ttf);;OpenType Fonts (*.otf)");
        QMetaObject::invokeMethod(&m_rawFontRenderer,
                                  [&]() { m_rawFontRenderer.loadFontFromFilePath(filePath); });
    });
    ui->lineEditFontPath->setAcceptDrops(true);
    ui->comboBoxUtfCategory->setModel(new QStringListModel(bfontview::UTF16PlaneNames));
    connect(ui->comboBoxUtfCategory, &QComboBox::activated, this, [&](int id) {
        ui->listViewGlyphs->scrollTo(
            ui->listViewGlyphs->model()->index(bfontview::UTF16PlaneRanges[id].start, 0));
    });
    connect(&m_rawFontRenderer, &RawFontRenderer::fontLoaded, this, &BFontViewer::fontLoaded);
    m_ucdLoaderThread.reset(new QThread(this));
    ucdLoaderInstance()->moveToThread(m_ucdLoaderThread.get());
    ucdLoaderInstance()->loadDB();
    m_rawFontRendererThread.reset(new QThread(this));
    m_rawFontRendererThread->start();
    QMetaObject::invokeMethod(&m_rawFontRenderer, [&](){
        m_rawFontRenderer.loadFontFromSystem
                (ui->fontComboBox->currentFont().family());
    });
    connect(ui->buttonFind, &QPushButton::pressed,
            this,&BFontViewer::findGlyphFromInputTerms);
    m_glyphModelFilter.setDynamicSortFilter(true);
    m_glyphModelFilter.setFilterCaseSensitivity(
                Qt::CaseInsensitive);
    m_glyphModelFilter.setSourceModel(m_rawFontRenderer.model());
    m_glyphModelFilter.setFilterKeyColumn(
                RawFontGlyphModel::DisplayRoleColumns::DESCRIPTION);
    m_glyphModelFilter.setFilterFixedString("");
    connect(ui->inputSearchTerms, &QLineEdit::returnPressed,
            this, &BFontViewer::findGlyphFromInputTerms);
    ui->treeViewResults->setModel(&m_glyphModelFilter);
    ui->treeViewResults->hideColumn(0);
    ui->treeViewResults->hideColumn(1);
    setAcceptDrops(true);
}

BFontViewer::~BFontViewer()
{
    m_rawFontRenderer.moveToThread(thread());
    m_rawFontRendererThread->quit();
    m_rawFontRendererThread->wait();
    delete ui;
}

void BFontViewer::updateUTFCategoryComboBoxSelectedItem(int id)
{
    //find the matching utf-16 plane, update the plane combobox
    for (size_t planeIter = 0; planeIter < bfontview::UTF16PlaneRanges.size(); planeIter++) {
        if (id >= bfontview::UTF16PlaneRanges[planeIter].start
            && id <= bfontview::UTF16PlaneRanges[planeIter].end) {
            ui->comboBoxUtfCategory->setCurrentIndex(planeIter);
        }
    }
}

void BFontViewer::findGlyphFromInputTerms()
{
    if (ui->selectDescription->isChecked()) {
        m_glyphModelFilter.setFilterKeyColumn(
                    RawFontGlyphModel::DisplayRoleColumns::DESCRIPTION);
    } else if (ui->selectLiteral->isChecked()) {
        m_glyphModelFilter.setFilterKeyColumn(
                    RawFontGlyphModel::DisplayRoleColumns::UTF16LITERAL);
    } else if (ui->selectUtfCode->isChecked()) {
        bool valid = false;
        quint32 utfCode = ui->inputSearchTerms->text().toUInt(&valid, 16);
        if (!valid) {
            return;
        }
        QModelIndex targetIndex = ui->listViewGlyphs->model()->index(utfCode, 0);
        ui->listViewGlyphs->selectionModel()
            ->setCurrentIndex(targetIndex, QItemSelectionModel::SelectionFlag::SelectCurrent);
        ui->listViewGlyphs->scrollTo(targetIndex);
        ui->listViewGlyphs->selectionModel()->clearSelection();
        ui->listViewGlyphs->selectionModel()->select(targetIndex,
                                                     QItemSelectionModel::SelectionFlag::Select);
        //m_glyphModelFilter.setFilterKeyColumn(RawFontGlyphModel::DisplayRoleColumns::CODEPOINT);
    }
    m_glyphModelFilter.setFilterWildcard(ui->inputSearchTerms->text());
}

//update the glyphs list box model
void BFontViewer::fontLoaded(RawFontGlyphModel* glyphsModel)
{
    Q_UNUSED(glyphsModel);
    setWindowTitle(QString("%1 - bfontview").arg(m_rawFontRenderer.fontFamilyName()));
//    QPixmap glyphImage(256,256);
}

void BFontViewer::dragEnterEvent(QDragEnterEvent *event)
{
    qDebug() << event->mimeData()->urls()[0];
    qDebug() << event->mimeData()->formats()[0];
    if (event->mimeData()->urls().size() == 1)  {
        event->acceptProposedAction();
    }
}

void BFontViewer::dropEvent(QDropEvent *event)
{
    qDebug() << event->mimeData()->urls()[0];
    QString localFilePath = event->mimeData()->urls()[0].toLocalFile();
    ui->lineEditFontPath->setText(localFilePath);
    QMetaObject::invokeMethod(&m_rawFontRenderer, [&](){
        m_rawFontRenderer.loadFontFromFilePath(localFilePath);
    });
    event->acceptProposedAction();
}

