from PyQt5.QtCore import QByteArray, QBuffer, QIODevice, qCompress
import zlib

# Open the file for reading
with open("ucd.txt", "rb") as f:
    data = f.read()

# Compress the file contents with qCompress
compressed_data = QByteArray(data)
compressed_data = qCompress(compressed_data)

# Write the compressed data to a file
with open("ucd.txt.gz", "wb") as f:
    f.write(compressed_data)
