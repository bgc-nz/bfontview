## BFontView
![Semantic description of image](/screenshots/bfontview-windows-11.png "bfontview screenshot on Windows 11")

Ben's font viewer, display and search for UTF-16 glyphs. Uses the official Unicode standard 
database in zlib compressed format.

Building this program requires Qt 5.15 or newer. Has been tested on Windows 11 Professional, and Ubuntu 22.04
running with WSL.

## Authors
BFontView program written by Ben Cottrell, UCD data derived from ucd.flat.xml https://www.unicode.org/Public/14.0.0/ucdxml/.

## File Structure
### bfontviewer.{h,cpp}
Main application code.
### bfontview.pro
Used to generate build system files, such as Makefiles on GNU Linux.
### main.cpp
Entrypoint, initializes QApplication, starts the event loop, and creates BFontViewer.
### ucd.txt.gz
Used to create the header ucd_zcomp.h, with a command line similar to:
```
python ucd-comp.py && xxd -i ./ucd.txt.gz > ucd_zcomp.h \
&& echo -e "unsigned int ucd_txt_orig_size=" $(stat -c ucd.txt) ";" >> ucd_zcomp.h 
```
### ucd-comp.py
A script for compressing ucd.txt. 
Unfortunately, this requires the PyQt5 package for Python, as I couldn't figure out how to prepend
a correct header to compressed data produced by the builtin zlib module.
### bfontviewer.ui
Defines the user interface for the main window. Transformed into a source file at compile time,
and made available for the class BFontViewer.
### rawfontrenderer.{h,cpp}
Definition and implementation for raw font rendering, populating a model class with GlyphInfo entries,
which contain the codepoint, painter path and UTF-16 literal value.
The model class renders the path when the DecorationRole is requested to the overloaded function
for QAbstractItemModel::data.