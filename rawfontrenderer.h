#ifndef RAWFONTRENDERER_H
#define RAWFONTRENDERER_H
#include <QObject>
#include <QThread>
#include <QRawFont>
#include <QPixmap>
#include <QAbstractItemModel>
#include <QPainterPath>
#include "ucdloader.h"

//This is used for the list view, so we don't have to create
//QListWidgetItem's from GlyphInfo's, freeing the main UI thread
class RawFontGlyphModel : public QAbstractItemModel
{
    // QAbstractItemModel interface
public:
    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &child) const;
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;

    struct GlyphInfo {
        QPainterPath path;
        uint16_t codepoint;
        QString utf16Literal;
    };
    enum DisplayRoleColumns {
       UTF16LITERAL,
       CODEPOINT,
       DESCRIPTION
    };
    const GlyphInfo& glyphInfoAt(const int index);
    const int kMaxCols = 3;
    using GlyphList = QVector<GlyphInfo>;
    friend class RawFontRenderer;
private:
    GlyphList m_glyphs;
};

//This class does rendering of a raw font, to be
//executed on a separate thread
class RawFontRenderer : public QObject
{
    Q_OBJECT
public:
    void loadFontFromFilePath(const QString&);
    void loadFontFromSystem(const QString&);
    RawFontGlyphModel* model() { return &m_glyphModel; }
    const QString fontFamilyName() { return m_rawFont.familyName(); }
protected:
    void generateGlyphs();
signals:
    void fontLoaded(RawFontGlyphModel*);
private:
    const QSize kPixmapSize{64,64};
    const double kFontSize = kPixmapSize.width()*0.48;
    RawFontGlyphModel m_glyphModel;
    QRawFont m_rawFont;
};

#endif // RAWFONTRENDERER_H
