QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    bfontviewer.cpp \
    rawfontrenderer.cpp \
    ucdloader.cpp

HEADERS += \
    bfontviewer.h \
    rawfontrenderer.h \
    ucdloader.h \
    unicoderanges.h \
    ucd_zcomp.h

FORMS += \
    bfontviewer.ui

TRANSLATIONS += \
    bfontview_en_001.ts
CONFIG += lrelease
CONFIG += embed_translations
VERSION = 1.17
win32 {
    QMAKE_TARGET_PRODUCT = BFontView
    QMAKE_TARGET_DESCRIPTION = Unicode character search tool
    QMAKE_TARGET_COPYRIGHT = "Copyright © 2023 by Ben Cottrell"
    RC_ICONS = app-icon-win.ico
}

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=
