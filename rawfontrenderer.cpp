#include "rawfontrenderer.h"
#include <QPainter>
#include <QPainterPath>
#include <QIcon>
#include <QtConcurrent/QtConcurrentMap>


void RawFontRenderer::loadFontFromFilePath(const QString &path)
{
    m_rawFont.loadFromFile(path, kFontSize,
                           QFont::HintingPreference::PreferDefaultHinting);
    generateGlyphs();
}

void RawFontRenderer::loadFontFromSystem(const QString &name)
{
    QFont f(name);
    f.setPixelSize(kFontSize);
    m_rawFont = QRawFont::fromFont(f);
    generateGlyphs();
}

void RawFontRenderer::generateGlyphs()
{
    QPainter p;
    m_glyphModel.beginInsertRows(m_glyphModel.index(0,0, QModelIndex()), 0, 0x7FFD);
    m_glyphModel.m_glyphs.clear();
    for (uint32_t t=0; t<0x7FFD; t++) {
        auto uc = QChar(t);
        RawFontGlyphModel::GlyphInfo g;
        auto glyphIndex = m_rawFont.glyphIndexesForString(QString(uc))[0];
        g.path = m_rawFont.pathForGlyph(glyphIndex);
        g.codepoint = t;
        g.utf16Literal = QString(uc);
        m_glyphModel.m_glyphs.append(g);
    }
    m_glyphModel.endInsertRows();
    emit fontLoaded(&m_glyphModel);
}

QModelIndex RawFontGlyphModel::index(int row, int column, const QModelIndex &parent) const
{
    if (column < kMaxCols) {
        return QAbstractItemModel::createIndex(row, column);
    }
    return QModelIndex();
}

QModelIndex RawFontGlyphModel::parent(const QModelIndex &child) const
{
    return QModelIndex();
}

int RawFontGlyphModel::rowCount(const QModelIndex &parent) const
{
    return m_glyphs.count();
}

int RawFontGlyphModel::columnCount(const QModelIndex &parent) const
{
    return kMaxCols;
}

QVariant RawFontGlyphModel::data(const QModelIndex &index, int role) const
{
    QVariant value;
    const int c = index.column(), r= index.row();
    if (index.column() < kMaxCols) {
        switch (role) {
        case Qt::DisplayRole:
            if (c == 0) {
                value = m_glyphs[r].utf16Literal;
            } else if (c == 1) {
                value = m_glyphs[r].codepoint;
            } else if (c == 2) {
                value = ucdLoaderInstance()->codePointName(m_glyphs[r].codepoint);
            }
            break;
        case Qt::DecorationRole: {
            QPixmap icon(40, 40);
            icon.fill();
            QPainter p;
            GlyphInfo gi = m_glyphs[r];
            QRectF br = gi.path.boundingRect();
            p.begin(&icon);
            p.setRenderHint(QPainter::RenderHint::Antialiasing);
            p.translate(icon.width()*0.5-br.center().x(),
                        icon.height()*0.5-br.center().y());
            p.fillPath(gi.path, QBrush(QColor(Qt::black)));
            p.end();
            value = icon;
            }
        }
    }
    return value;
}

const RawFontGlyphModel::GlyphInfo& RawFontGlyphModel::glyphInfoAt(const int index)
{
    return m_glyphs[index];
}


QVariant RawFontGlyphModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    QVariant value;
    if (role == Qt::DisplayRole) {
        switch (section) {
        case 0:
            value = "UTF-16 Literal";
            break;
        case 1:
            value = "Codepoint";
            break;
        case 2:
            value = "Description";
        }
    }
    return value;
}


Qt::ItemFlags RawFontGlyphModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemNeverHasChildren;
}
